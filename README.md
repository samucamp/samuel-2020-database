# Samuel 2020 - Banco de Dados

Aprendendo uso de banco de dados relacionais e não relacionais de diversas fontes.

<br>

# Conteúdos
1. [Instalando MySQL no UBUNTU 18.04](#mysqlinstall)
2. [Aqui deve ter um tutorial para aumentar a segurança.](#mysqlsecurity)
3. [Possível problema (Error 1698-28000)](#problem)
4. [Desinstalando MySQL](#mysqlremove)
5. [Referências](#ref)

<br>

# Instalando MySQL no UBUNTU 18.04
<a name="mysqlinstall"></a>

## Instalando MySQL Workbench.

Download [MySQL Workbench](https://dev.mysql.com/downloads/workbench/)

MySQL Workbench is a visual database design tool that integrates SQL development, administration, database design, creation and maintenance into a single integrated development environment for the MySQL database system.

<br> 

## Instalando MySQL Server and Client.

```bash
sudo apt-get update 
sudo apt-get install mysql-server 
sudo apt-get install mysql-client
mysql_secure_installation
```

<br>

## Após instalação, testar com:

```bash
systemctl status mysql.service
```

<br>

## Conectando ao MySQL, se necessário utilizar <code>sudo</code>.

```bash
mysql -u root -p
```

<br>

# <code>Aqui deve ter um tutorial para aumentar a segurança.</code>
<a name="mysqlsecurity"></a>

<code>Testar e explicar conforme tutorial na [digitalocean](https://www.digitalocean.com/community/tutorials/how-to-secure-mysql-and-mariadb-databases-in-a-linux-vps)</code>

```bash

```

<br>


## Verificando User, Host, Plugin

```bash
sudo mysql -u root # talvez seja necessário root em instalação nova

mysql> USE mysql;
mysql> SELECT User, Host, plugin FROM mysql.user;

+------------------+-----------------------+
| User             | plugin                |
+------------------+-----------------------+
| root             | auth_socket           |
| mysql.sys        | mysql_native_password |
| debian-sys-maint | mysql_native_password |
+------------------+-----------------------+
```

<br>

# Possível problema (Error 1698-28000) 
<a name="problem"></a>

<code>Testar e explicar opções abaixo conforme resposta no [stackoverflow](https://stackoverflow.com/questions/39281594/error-1698-28000-access-denied-for-user-rootlocalhost)

Reesolução do problema error-1698-28000-access-denied-for-user-rootlocalhost</code>

**Opção 1**

You can set the root user to use the mysql_native_password plugin

```bash
sudo mysql -u root # Need to use "sudo" since is new installation

mysql> USE mysql;
mysql> UPDATE user SET plugin='mysql_native_password' WHERE User='root';
mysql> FLUSH PRIVILEGES;
mysql> exit;

service mysql restart
```

**Opção 2**

You can create a new db_user with you system_user (recommended)
(replace YOUR_SYSTEM_USER with the username you have)

```bash
sudo mysql -u root # I had to use "sudo" since is new installation

mysql> USE mysql;
mysql> CREATE USER 'YOUR_SYSTEM_USER'@'localhost' IDENTIFIED BY '';
mysql> GRANT ALL PRIVILEGES ON *.* TO 'YOUR_SYSTEM_USER'@'localhost';
mysql> UPDATE user SET plugin='auth_socket' WHERE User='YOUR_SYSTEM_USER';
mysql> FLUSH PRIVILEGES;
mysql> exit;

service mysql restart
```

<br>

# Desinstalando MySQL
<a name="mysqlremove"></a>

```bash
sudo apt-get remove --purge mysql*
sudo apt-get autoremove
sudo apt-get autoclean

sudo apt-get purge mysql-server mysql-client mysql-common mysql-server-core-5.7 mysql-client-core-5.7
sudo apt-get autoremove
sudo apt-get autoclean
sudo rm -rf /var/lib/mysql
sudo rm -rf /etc/mysql
```

<br>

# Referências
<a name="ref"></a>

>https://dev.mysql.com/downloads/workbench/

>https://www.digitalocean.com/community/tutorials/como-instalar-o-mysql-no-ubuntu-18-04-pt

>https://www.digitalocean.com/community/tutorials/how-to-secure-mysql-and-mariadb-databases-in-a-linux-vps

> https://blog.softhints.com/install-reinstall-uninstall-mysql-on-ubuntu-16/#uninstall

> https://stackoverflow.com/questions/39281594/error-1698-28000-access-denied-for-user-rootlocalhost